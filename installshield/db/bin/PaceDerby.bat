@echo off
setlocal

rem Development Group script block
rem 
rem This block checks for derby already listening on the default port and exits

set PACE_DERBY_PORT=1527
set PACE_DERBY_HOST=localhost

portqry -n %PACE_DERBY_HOST% -p tcp -e %PACE_DERBY_PORT% -q
SET PORT_STATUS=%errorLevel%

if %PORT_STATUS%==0 ECHO Derby port (%PACE_DERBY_PORT%) in use on %PACE_DERBY_HOST%. & goto :exitbat
rem End port test block



rem Copyright (c) 1999, 2006 Tanuki Software Inc.
rem
rem Java Service Wrapper general startup script
rem

rem
rem Resolve the real path of the wrapper.exe
rem  For non NT systems, the _REALPATH and _WRAPPER_CONF values
rem  can be hard-coded below and the following test removed.
rem
if "%OS%"=="Windows_NT" goto nt
echo This script only works with NT-based versions of Windows.
goto :eof

:nt
rem
rem Find the application home.
rem
rem %~dp0 is location of current script under NT
set _REALPATH=%~dp0



rem Decide on the wrapper binary.
set _WRAPPER_BASE=wrapper
set _WRAPPER_EXE=%_REALPATH%%_WRAPPER_BASE%-windows-x86-32.exe
if exist "%_WRAPPER_EXE%" goto conf
set _WRAPPER_EXE=%_REALPATH%%_WRAPPER_BASE%-windows-x86-64.exe
if exist "%_WRAPPER_EXE%" goto conf
set _WRAPPER_EXE=%_REALPATH%%_WRAPPER_BASE%.exe
if exist "%_WRAPPER_EXE%" goto conf
echo Unable to locate a Wrapper executable using any of the following names:
echo %_REALPATH%%_WRAPPER_BASE%-windows-x86-32.exe
echo %_REALPATH%%_WRAPPER_BASE%-windows-x86-64.exe
echo %_REALPATH%%_WRAPPER_BASE%.exe
pause
goto :eof

:extcalls
rem this line will invoke a utility to update the last period
rem in the server based transfer directory. If the client
rem chooses to update this file automatically, calling this
rem utility will update the paf_apps.xml to reflect the new
rem current time period.
rem NOTE!! this command line is NOT invoked if the
rem PafServer is installed as a service. Another
rem mechanism must be developed to automatically call
rem "updateElapsedPeriods.bat" after data loads/server bounces
rem call "../../bin/updateElapsedPeriods.bat"



rem
rem Find the wrapper.conf
rem
:conf
set _WRAPPER_CONF="%~f1"
if not %_WRAPPER_CONF%=="" goto startup
set _WRAPPER_CONF="%_REALPATH%wrapper.conf"

rem
rem Start the Wrapper
rem
:startup
"%_WRAPPER_EXE%" -c %_WRAPPER_CONF%
if not errorlevel 1 goto :eof
pause

:exitbat
exit

