#!/bin/sh

ln -fs `pwd`/pacederby.sh /etc/init.d/pacederby
chmod 777 /etc/init.d/pacederby

#chkconfig --add pacederby
#chkconfig --levels 2345 pacederby on
#chkconfig --levels 016 pacederby off

#update-rc.d pacederby defaults
update-rc.d pacederby start 20 2 3 4 5 . stop 20 0 1 6 .
#update-rc.d -f pacederby  remove

#chmod +x /pacederby.sh
#update-rc.d/pacederby defaults
