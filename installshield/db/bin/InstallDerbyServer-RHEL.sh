#!/bin/sh

ln -fs `pwd`/pacederby.sh /etc/rc.d/init.d/pacederby
chmod 777 /etc/rc.d/init.d/pacederby

chkconfig --add pacederby
chkconfig --levels 2345 pacederby on
chkconfig --levels 016 pacederby off
