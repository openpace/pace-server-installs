ln -fs `pwd`/paceserver.sh /etc/rc.d/init.d/paceserver
chmod 777 /etc/rc.d/init.d/paceserver

chkconfig --add paceserver
chkconfig --levels 2345 paceserver on
chkconfig --levels 016 paceserver off
