#!/bin/sh

ln -fs `pwd`/paceserver.sh /etc/init.d/paceserver
chmod 777 /etc/init.d/paceserver

#chkconfig --add paceserver
#chkconfig --levels 2345 paceserver on
#chkconfig --levels 016 paceserver off

#update-rc.d paceserver defaults
update-rc.d paceserver start 20 2 3 4 5 . stop 20 0 1 6 .
#update-rc.d -f paceserver  remove
