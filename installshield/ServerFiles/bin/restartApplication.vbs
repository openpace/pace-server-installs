' Copyright (c) 2017-2019 Contributors to Open Pace and others.
 
' The OPEN PACE product suite is free software: you can redistribute it and/or
' modify it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or (at your
' option) any later version.
 
' This software is distributed in the hope that it will be useful, but WITHOUT
' ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
' or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
' License for more details.
 
' You should have received a copy of the GNU General Public License along with
' this  software. If not, see <http://www.gnu.org/licenses/>.

'=================================================
' Script to restart a Pace application.
'
' Parameters
' #1: (Required) - Name of the Pace application (Case Sensitive)
' #2: (Required) - URL to the Pace server. 
'
' Written by: Karl Moos
' Last Revision: 1.0 (08/22/2014)
'================================================
'The SOAP message that will be sent.
Private Const SOAP_MESSAGE = "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:pace=""open-pace.com""><soapenv:Header/><soapenv:Body><pace:startApplication><arg0><clientId>0</clientId><sessionToken>0</sessionToken><appIds>%%APPID%%</appIds></arg0></pace:startApplication></soapenv:Body></soapenv:Envelope>"
'Token to be replaced with real App Id
Private Const APP_ID_TOKEN = "%%APPID%%"
'Verbose flag for debugging.
Private Const VERBOSE = false
'Main function to run script
Call Main()

'*********************************************************
 ' Purpose:  Meat of the script
 '*********************************************************
Sub Main()
	dim appId, url, oHttp, strSoapReq, strResult, soapMessage, result

	'Log that we are running.
	WriteToConsole "Starting restartApplication.vbs"
	
	'Make sure the user specified at least 1 parm
	if WScript.Arguments.Count = 0 then
		WriteToConsole "Missing parameters"
		WScript.Quit -1
	end if
	'Make sure the user didn't specify more than 2 parms.
	if WScript.Arguments.Count > 2 then
		WriteToConsole "Invalid number of parameters"
		WScript.Quit -1
	end if
	
	'Extract the parameters
	appId = WScript.Arguments(0)
	url	= WScript.Arguments(1)

	if VERBOSE then
		WriteToConsole "Using appId: " + appId
		WriteToConsole "Using server: " + url
	end if
	
	' Create the http request text
	strSoapReq = Replace(SOAP_MESSAGE, APP_ID_TOKEN, appId)
	Set oHttp = CreateObject("Msxml2.XMLHTTP")
	
	oHttp.open "POST", URL, false

	oHttp.setRequestHeader "Content-Type", "text/xml"
	'Send the SOAP message
	On Error Resume Next
	oHttp.send strSoapReq
	if Err.Number <> 0 then
		if Err.Number = -2146697211 then
			WriteToConsole "URL: [" + url + "] cannot be found.  Please double check the server name, port, and service name"
		else
			WriteToConsole "Error # " + CStr(Err.Number) + " : " + Err.Description
		end if
		WScript.Quit  -1
	end if
	On Error GoTo 0
	'Get the SOAP message response
	strResult = oHttp.responseText
	if VERBOSE then
		WriteToConsole strResult
	end if
	
	'Parse the response.
	result = Trim(GetResult(strResult, "success"))
	if VERBOSE then
		WriteToConsole "Success: " + result
	end if
	
	dim bSuccess
	if Len(result) > 0 then
		bSuccess = cbool(result)
	else
		bSuccess = false
	end if
	
	
	'Nicely format the SOAP response
	dim displayMessage
	if bSuccess then
		displayMessage = "Application [" + appId + "] was restarted successfully."
	else
		Dim msg
		msg = GetResult(strResult, "responseMsg")
		if VERBOSE then
			WriteToConsole msg
		end if
		if Len(msg) = 0 then
			displayMessage = "Application [" + appId + "] was not restarted successfully, server returned: " + strResult
		else
			displayMessage = "Application [" + appId + "] was not restarted successfully, server returned: " + msg
		End if		
		
	end if
	
	WriteToConsole displayMessage
	
	WScript.Quit 0

End Sub

'*********************************************************
 ' Purpose:  Parses an XML string and returns the value specified in the strNode
 ' Inputs:   strXmlText:   The xml string to parse
 '           strNode:   Node to find and return value of
 ' Returns:  The value of strNode or nothing if the node is not found.
 '*********************************************************
Function GetResult(byval strXmlText, byval strNode)

    Dim oXml

    Set oXml = CreateObject("Msxml2.DOMDocument")

    oXml.Async = true

    oXml.LoadXml strXmlText

    Set objNodeList = oXml.getElementsByTagName(strNode)

	If objNodeList.length > 0 then
		For each x in objNodeList
			GetResult = x.Text
			exit function
		Next
	Else
		GetResult = ""
	End If

End Function 

'*********************************************************
 ' Purpose:  Writes a log message to the Wscript console
 ' Inputs:   strText:  Text to print
 '*********************************************************
Sub WriteToConsole(byval strText)

	WScript.StdOut.WriteLine strText

End Sub