#!/bin/bash

# Copyright (c) 2017-2019 Contributors to Open Pace and others.
 
# The OPEN PACE product suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
 
# This software is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
 
# You should have received a copy of the GNU General Public License along with
# this  software. If not, see <http://www.gnu.org/licenses/>.

#=================================================
# Script to restart a Pace application.
# 
# Parameters
# #1: (Required) - Name of the Pace application (Case Sensitive)
# #2: (Optional) - URL to the Pace server.  If not specified "http://localhost:8080/pace/app" is used.
#
# Written by: Karl Moos
# Last Revision: 1.0 (08/22/2014)
#================================================

#Variable to hold the temp reponse file
TEMP_FILE=""
#Variable for the default server url
PACE_SERVER="http://localhost:8080/pace/app"
#Set this to true to get verbose debugging information
VERBOSE=false

#Function to delete file
deleteFile() {
	if [ -e "$TEMP_FILE" ]; then
		rm -f $TEMP_FILE
	fi
}

displayHelp(){
	echo "Usage is $0 applicationId [Server URL]"
}

verboseOutput(){
	if [ "$VERBOSE" = true ]; then
  		echo ${1}
	fi
}

#Make sure the user specified the app id
if [[ $# -eq 0 ]] ; then
    echo "The Pace Application Name paramater is required."
    displayHelp
    exit 1
fi

#Output parameters
if [[ $# -eq 1 ]] ; then
	appId=$1
	echo "Restarting Pace application: $appId"
    echo "Using default Pace server url: ${PACE_SERVER}"
fi

#User specified a server URL, so set it!
if [[ $# -gt 1 ]] ; then
    appId=$1
    PACE_SERVER=$2
	echo "Restarting Pace application: $appId"
    echo "Using Pace server url: $PACE_SERVER"
fi

#User specified too many parms, warn them and exit.
if [[ $# -gt 2 ]] ; then
    echo "Only two parameters are allowed."
    displayHelp
    exit 1
fi

verboseOutput "Using appId: $appId"
verboseOutput "Using url: $PACE_SERVER"

TEMP_FILE=$(mktemp)
verboseOutput "Using temp file: $TEMP_FILE"

#Create the SOAP message to restart the application.
Soap_Message=`cat <<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pace="open-pace.com">
   <soapenv:Header/>
   <soapenv:Body>
      <pace:startApplication>
         <arg0>
            <clientId>0</clientId>
            <sessionToken>0</sessionToken>
            <appIds>$1</appIds>
         </arg0>
      </pace:startApplication>
   </soapenv:Body>
</soapenv:Envelope>
EOF
`
#Send the SOAP message to the server using CURL
curlResponse=$(curl \
--header "Content-Type: text/xml; charset=utf-8" \
--header "SOAPAction:" \
--data "${Soap_Message}" \
--request POST $PACE_SERVER)

# use "--connect-timeout" to specify a timeout

verboseOutput "curl response: ${curlResponse}"

#Write the server SOAP response to a temp file.
echo "${curlResponse}" >> $TEMP_FILE

#Get the success reponse from the xml file.
response=$(grep -Po '(?<=<success>)\w+(?=</success>)' $TEMP_FILE)

#Check if the success node was found and get the value.
bSuccess="false"
if [ ${#response} -gt 0 ]; then
	bSuccess=${response}
fi

#Check to see if the restart was successful
if [ "$bSuccess" == "true" ]; then
	echo "Application [${appId}] was restarted successfully."
	#Purge the file
	deleteFile
	#Return 0
	exit 0
else
	#Get the error the xml file.
	errorMsg=$(grep -Po '(?<=<responseMsg>).*?(?=</responseMsg>)' $TEMP_FILE)
	verboseOutput "error message: ${errorMsg}"
	if [ ${#errorMsg} -eq 0 ]; then
		echo "Application was not restarted successfully, server returned: $(<$TEMP_FILE)"
	else
		echo "Application was not restarted successfully, server returned: $errorMsg"
	fi
	#Purge the temp file
	deleteFile
	#Return non 0
	exit 1
fi