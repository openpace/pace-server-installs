@echo off

REM Copyright (c) 2017-2019 Contributors to Open Pace and others.
 
REM The OPEN PACE product suite is free software: you can redistribute it and/or
REM modify it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or (at your
REM option) any later version.
 
REM This software is distributed in the hope that it will be useful, but WITHOUT
REM ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
REM or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
REM License for more details.
 
REM You should have received a copy of the GNU General Public License along with
REM this  software. If not, see <http://www.gnu.org/licenses/>.


rem =================================================
rem Script to restart a Pace application.
rem Calls restartApplication.vbs
rem Parameters
rem #1: (Required) - Name of the Pace application (Case Sensitive)
rem #2: (Optional) - URL to the Pace server.  If not specified "http://localhost:8080/pace/app" is used.
rem
rem Written by: Karl Moos
rem Last Revision: 1.0 (08/22/2014)
rem ================================================

REM Default variable for the default server url
set PACE_SERVER="http://localhost:8080/pace/app"

REM Get the total number of agruments passed to script.	
call :getargc argc %*

REM Make sure the user specified the app id
if %argc% equ 0 goto :noParms

REM User specified too many parms, warn them and exit.
if %argc% gtr 2 goto :tooManyParms

REM Set the application ID
set appId=%1
echo Restarting Pace application: %appId%

REM Process the server url
if %argc% equ 1 goto setParmsServer
	set PACE_SERVER=%2
    echo Using default Pace server url: %PACE_SERVER%	
:setParmsServer

REM Call the vbs script to send the SOAP Message
pushd %~dp0
cscript restartApplication.vbs %appId% %Pace_Server%
goto :eof-ok


:getargc
    set getargc_v0=%1
    set /a "%getargc_v0% = 0"
:getargc_l0
    if not x%2x==xx (
        shift
        set /a "%getargc_v0% = %getargc_v0% + 1"
        goto :getargc_l0
    )
    set getargc_v0=
    goto :eof
	
:noParms
	echo The Pace Application Name paramater is required.
	goto :displayHelp
	
:tooManyParms
	echo Only two parameters are allowed.
	goto :displayHelp

:displayHelp
	echo Usage is startme.bat applicationId [Server URL]
	goto :eof-error

:eof-ok	
	echo Exiting...
	Exit /b 0
	
:eof-error
	echo Exiting...
	Exit /b -1

	
