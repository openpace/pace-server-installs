##!/bin/bash

#Skip creating tablespaces and schema step if they are not needed

host=
sid=

:<<create_tables

user=SYSTEM
password=Pafsys123
sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @Create_Tablespaces_Linux.sql

sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @Create_Schema.sql

create_tables

user=PAFSECURITY
password=Pafsys123
sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @PaceSecurity_DDL.sql

user=PAFCLIENTCACHE
password=Pafsys123
sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @PaceClientCache_DDL.sql

user=PAFEXTATTR
password=Pafsys123
sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @PaceExtAttr_DDL.sql

user=PAFSECURITY
password=Pafsys123
sqlplus $user/$password@\(DESCRIPTION=\(ADDRESS_LIST=\(ADDRESS=\(PROTOCOL=TCP\)\(HOST=$host\)\(PORT=1521\)\)\)\(CONNECT_DATA=\(SID=$sid\)\)\) @PaceSecurity_LoadData.sql



