#!/bin/sh

rm /etc/init.d/paceserver

# remove serivce (Debian only)
update-rc.d -f paceserver  remove

#delete link in services dir
rm -f /etc/rc.d/init.d/paceserver

#rm -r bin conf data lib redist transfer
rm -r `pwd`/*
#cd server
#rm -r appclient bin bundles

#remove service (RedHat/CentOS)
chkconfig --del paceserver