 #!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use File::Path;

my $userHome = $ENV{'HOME'};

$SIG{'INT'} = 'IGNORE';
EULA_LOC:
system("clear");
print "*** Welcome to the Open Pace Server Installation for Linux ***\n";
print "Version 3.0.1.1\n\n";
print "Please read the End User License Agreement located:\n";
my $licensePagePath = "wiki/spaces/PPD300/pages/162988084/License";
print "https://pace-planning.atlassian.net/$licensePagePath\n\n";
print "Do you accept the license agreement? \n(Y or Q to Quit):";
my $input = UserInput("Y", "Q");

INSTALL_LOC:
system("clear");
print "*** Server Installation ***\n\n";
print "The default installation location is /opt/open-pace/\n\n";
print "Accept this default location? \n(Y/N, B to go Back, or Q to Quit): ";
my $input = UserInput("Y", "N", "B", "Q");
if($input =~ /b/i)
{
  goto EULA_LOC;
}

my $cnt = 0;
my $dir = "/opt/open-pace/";
if($input =~ /n/i)
{
  for($cnt = 0; $cnt < 5; $cnt++)
  {
    print "\n\nPlease enter the fully qualified desired installation path: ";
    my $tempdir = <STDIN>;
    $tempdir =~ s/[\n\r\f\t]//g;

    my $last = substr $tempdir,-1,1;

    if($last ne "/") 
    {
      $tempdir = $tempdir . "/";
    }

    $last = substr $tempdir,0,1;
    if($last ne "/") 
    {
      $tempdir = "/" . $tempdir;
    }

    print "Confirm $tempdir (Y/N or Q to Quit): ";
    my $tempanswer = "n";
    $tempanswer = UserInput("Y", "N", "Q");
    if($tempanswer =~ /y/i)
    {
      $dir = $tempdir;
      last;
    }
  }

  $cnt < 5 or die "Please run the installer again.";
}

my $derbydir = $dir . "derby";
my $serverdir = $dir . "pace-server";

my $ostype = `uname -a`; 
$ostype =~ s/\n//;

my $architecture = `uname -p`; 
$architecture =~ s/\n//;

my $numinstalls = CountSubDirs($serverdir);

my $servername = "SVR";
my $appname = "app";
my $jbossoffset = 0;
my $javaheap = "768";

if($numinstalls > 0)
{
  $servername = $servername . "$numinstalls";
  $appname = $appname . "$numinstalls";
  $jbossoffset = $numinstalls * 10000;
}
SERVER_INSTALL:
system("clear");
print "*** Server Installation ***\n\n";
print "The following are the recommended settings for this server installation...\n\n";
print "Unique Server Name: $servername\n";
print "Server App Name: $appname\n";
print "JBOSS Port Offset: $jbossoffset\n";
print "Maximum Java Heap Size (MB): $javaheap\n";
print "\nAccept the default recommended settings for this server \n(Y/N, B to go Back, or Q to Quit): ";
$input = UserInput("Y", "N", "Q", "B");

if($input =~ /b/i)
{
  goto INSTALL_LOC;
}
if($input =~ /n/i)
{
  $cnt = 1;
  while($cnt == 1)
  {
    print "\n*** Enter the number of the entity that needs to be changed or enter C to continue installation with the values shown below ***\n\n";
    print "1. Unique Server Name: $servername\n";
    print "2. Server App Name: $appname\n";
    print "3. JBOSS Port Offset: $jbossoffset\n";
	print "4. Maximum Java Heap Size (MB): $javaheap\n";
    print "C. Accept above values and continue\n";
    print "B. Back to previous installation step - Installation Location\n";
    print "Q. Quit installation\n";
    print "\nSelection: ";
    $input = UserInput("1", "2", "3", "4", "C", "B", "Q");

    if($input =~ /b/i)
    {
       goto INSTALL_LOC;
    }
    if($input =~ /c/i)
    {
       last;
    }

    $input =~ s/[\n\r\f\t]//g;
    if($input =~ /1/i)
    {
      print "Enter the value for the unique server name: ";
      $servername = <STDIN>;
      $servername =~ s/[\n\r\f\t]//g;
    }
    elsif($input =~ /2/i)
    {
      print "Enter the value for the server app name: ";
      $appname = <STDIN>;
      $appname =~ s/[\n\r\f\t]//g;
    }
    elsif($input =~ /3/i)
    {
      print "Enter the value for the JBOSS port offset: ";
      $jbossoffset = <STDIN>;
      $jbossoffset =~ s/[\n\r\f\t]//g;
    }
	elsif($input =~ /4/i)
	{
	  print "Enter the value for the max java heap size in MB: ";
	  $javaheap = <STDIN>;
	  $javaheap =~ s/[\n\r\f\t]//g;
	}
  }
}

my $serverservicename = "OPEN_PACE_APP_" . $servername;
$serverdir = $dir . "pace-server/$servername";
DERBY_INSTALL:
system("clear");
print "*** Derby Installation ***\n\n";
print "There are three options for Derby setup...\n\n";
print "1. No Derby installation\n";
print "2. New standalone Derby installation (recommend in a standard Pace installation)\n";
print "3. Share an existing Derby installation\n";
print "B. Back to previous installation step - Server Installation\n";
print "Q. Quit installation\n";
print "\nPlease choose an option: ";
$input = UserInput("1", "2", "3", "B", "Q");
system("clear");

$cnt = 0;
my $derbyname = $servername;
my $derbyDbSetName = $servername;
my $defDerbyName = "SVR";
my $derbyport = "1527";
my $derbyloc = "localhost";
my $derbyfileloc = "";
my $newderby = 0;
my $derbyservicename = $servername;
my $allowremotederby = "No";
my $noderby = 0;
my $sharedbs = 0;

if($input =~ /b/i)
{
  goto SERVER_INSTALL;
}
if($input =~ /3/i)
{
  $derbyname = $defDerbyName;
  $derbyDbSetName = $defDerbyName;
  $newderby = 0;
  print "*** Shared Derby Service Installation ***\n\n";  
  print "The following are the default settings for sharing an existing Derby installation...\n\n";
  print "Derby Server Network Location: $derbyloc\n";
  print "Derby Service Port: $derbyport\n";
  print "Derby Server Name: " . $derbyname . "\n";
  print "\nAccept the default settings for this Derby instance \n(Y/N, B to go Back, or Q to Quit): ";
  $input = UserInput("Y", "N", "Q", "B");

  if($input =~ /b/i)
  {
    goto DERBY_INSTALL;
  }
  if($input =~ /n/i)
  {
    $cnt = 1;
    while($cnt == 1)
    {
      print "\n*** Enter the number of the entity that needs to be changed or enter C to continue installation with the values shown below ***\n\n";
      print "1. Derby Server Network Location: $derbyloc\n";
      print "2. Derby Service Port: $derbyport\n";
	  print "3. Derby Server Name: " . $derbyname . "\n";
      print "C. Accept above values and continue\n";
      print "B. Back to previous installation step - Derby Installation\n";
      print "Q. Quit installation\n";
      print "\nSelection: ";
      $input = UserInput("1", "2", "3", "C", "B", "Q");

      if($input =~ /b/i)
      {
         goto DERBY_INSTALL;
      }
      if($input =~ /c/i)
      {
         last;
      }

      $input =~ s/[\n\r\f\t]//g;
      if($input =~ /1/i)
      {
        print "Enter the value for the derby server's network location: ";
        $derbyloc = <STDIN>;
        $derbyloc =~ s/[\n\r\f\t]//g;
      }
      elsif($input =~ /2/i)
      {
        print "Enter the value for the derby server's port: ";
        $derbyport = <STDIN>;
        $derbyport =~ s/[\n\r\f\t]//g;
      }
      elsif($input =~ /3/i)
      {
        print "Enter the value for the unique derby server name: ";
        $derbyname = <STDIN>;
        $derbyname =~ s/[\n\r\f\t]//g;
      }
   }
  }
  
  SHARED_DERBY_INSTALL:
  system("clear");
  print "*** Shared Derby Service Installation ***\n\n"; 
  print "Please choose whether to:\n";
  print "1. Share an existing set of derby databases\n";
  print "2. Create a new set of derby databases\n";
  print "B. Back to previous installation step - Derby Installation\n";
  print "Q. Quit installation\n";
  print "\nPlease choose an option: ";
  $derbydir = $dir . "derby/$derbyname";  
  $input = UserInput("1", "2", "B", "Q");
  if($input =~ /b/i)
  {
    goto DERBY_INSTALL;
  }
  if($input =~ /1/i)
  # Share an existing set of derby databases
  {
	$sharedbs = 1;
    $cnt = 1;
    while($cnt == 1)
    {
	  print "\n\n*** Share an existing set of derby databases (the default database set is: $defDerbyName) ***\n\n"; 
	  print "Please choose whether to:\n";
      print "1. Share the derby database set: $derbyDbSetName (fully qualified path is - $derbydir/data/$derbyDbSetName)\n";
	  print "2. Enter the name of different database set to share\n";
	  print "B. Back to previous installation step\n";
      print "Q. Quit installation\n";
	  print "\nPlease choose an option: ";
	  $input = UserInput("1", "2", "B", "Q");
      if($input =~ /b/i)
      {
		goto SHARED_DERBY_INSTALL;
	  }
      elsif($input =~ /1/i)
      {
		last;
	  }
      elsif($input =~ /2/i)
      {
        print "Please enter the name of the derby database set to share: ";
        $derbyDbSetName = <STDIN>;
        $derbyDbSetName =~ s/[\n\r\f\t]//g;
      }
    }
  }
  elsif($input =~ /2/i)
  {
    $sharedbs = 0;
    #move the dbs to the derby location
	for($cnt = 0; $cnt < 5; $cnt++)
    {
      print "\n\nPlease enter the full file location of the existing derby instance: ";
      my $tempname = <STDIN>;
      $tempname =~ s/[\n\r\f\t]//g;
	  
	  my $first = substr $tempname,0,1;
      if($first ne "/") 
      {
        $tempname = "/" . $tempname;
      }

      print "Confirm $tempname (Y/N or Q to Quit): ";
      my $tempanswer = "n";
      $tempanswer = UserInput("Y", "N", "Q");
      if($tempanswer =~ /y/i)
      {
        $derbyfileloc = $tempname;
        last;
      }
    }

    $cnt < 5 or die "Please run the installer again.";
  }
}
elsif($input =~ /1/i)
{
  $newderby = 0;
  $noderby = 1;
}
elsif($input =~ /2/i)
{
  $numinstalls = CountSubDirs($dir . "/derby");
  if($numinstalls > 0)
  {
    $derbyport = 1527 + $numinstalls;
  }
  $newderby = 1;
  print "*** New Derby Service Installation ***\n\n";
  print "The following are the recommended settings for this derby installation...\n\n";
  print "Unique Derby Server Name: $derbyname\n";
  print "Derby Service Port: $derbyport\n";
  print "Remote Connections Allowed: $allowremotederby\n";
  print "\nAccept the default recommended settings for this derby instance \n(Y/N, B to go Back, or Q to Quit): ";
  $input = UserInput("Y", "N", "Q", "B");

  if($input =~ /b/i)
  {
    goto DERBY_INSTALL;
  }
  if($input =~ /n/i)
  {
    $cnt = 1;
    while($cnt == 1)
    {
      print "\n*** Enter the number of the entity that needs to be changed or enter C to continue installation with the values shown below ***\n\n";
      print "1. Unique Derby Server Name: $derbyname\n";
      print "2. Derby Service Port: $derbyport\n";
	  print "3. Remote Connections Allowed: $allowremotederby\n";
      print "C. Accept above values and continue\n";
      print "B. Back to previous installation step - Derby Installation\n";
      print "Q. Quit installation\n";
      print "\nSelection: ";
      $input = UserInput("1", "2", "3", "C", "B", "Q");

      if($input =~ /b/i)
      {
         goto DERBY_INSTALL;
      }
      if($input =~ /c/i)
      {
         last;
      }

      $input =~ s/[\n\r\f\t]//g;
      if($input =~ /1/i)
      {
        print "Enter the value for the unique derby server name: ";
        $derbyname = <STDIN>;
        $derbyname =~ s/[\n\r\f\t]//g;
      }
      elsif($input =~ /2/i)
      {
        print "Enter the value for the derby port: ";
        $derbyport = <STDIN>;
        $derbyport =~ s/[\n\r\f\t]//g;
      }
	  elsif($input =~ /3/i)
	  {
	    print "Allow remote connections to this new derby instance?\nEnter Y for Yes or N for No: ";
		$input = UserInput("Y", "N");
		if($input =~ /y/i)
		{
		  $allowremotederby = "Yes";
		}
		else
		{
		  $allowremotederby = "No";
		}
	  }
    }
  }
 
  $derbyservicename = "OPEN_PACE_DB_" . $derbyname;
}
$derbydir = $dir . "derby/$derbyname";

system("clear");
#print out summary and go back to beginning if user does not accept
print "*** Installation Summary ***\n\n";
print "Installation location: " . $dir . "\n\n";
print "Server Installation\n";
print "- Server Directory: " . $serverdir . "\n";
print "- Server Name: " . $servername . "\n";
print "- App Name: " . $appname . "\n";
print "- JBOSS Port Offset: " . $jbossoffset . "\n";
print "- Maximum Java Heap Size (MB): " . $javaheap . "\n";
print "- Pace Server Service Name: " . $serverservicename . "\n";
print "\nDerby Installation\n";
if($newderby == 1)
{
  print "- New Derby Installation\n";
  print "- Derby Installation Directory: " . $derbydir . "\n";
  print "- Derby Server Name: " . $derbyname . "\n";
  print "- Derby Database Set Name: " . $derbyDbSetName . "\n";
  print "- Derby Service Name: " . $derbyservicename . "\n";
  print "- Remote Connections Allowed: " . $allowremotederby . "\n";
  print "- Derby Port: " . $derbyport . "\n";
}
else
{
  if($noderby == 1)
  {
    print "- No Derby Installation\n";
  }
  else
  {
    print "- Shared Derby Installation\n";
    print "- Derby Network Location: " . $derbyloc . "\n";
	print "- Derby Port: " . $derbyport . "\n";
    print "- Derby Server Name: " . $derbyname . "\n";
	if($sharedbs == 1)
	{
	  print "- Sharing Existing Derby Databases\n";
	  print "- Derby Database Set Name: " . $derbyDbSetName . "\n";
	}
	else
	{
      print "- Creating New Derby Databases\n";
	  print "- Derby Database Set Name: " . $derbyDbSetName . "\n";
	}
  }
}


print "\nAccept the above installation summary \n(Y/N, B to go Back, or Q to Quit): ";
$input = UserInput("Y", "N", "B", "Q");
if($input =~ /n/i)
{
  goto INSTALL_LOC;
}
if($input =~ /b/i)
{
  goto DERBY_INSTALL;
}
  
print "\nExtracting release to $serverdir\n";
unless (-d $serverdir)
{
  mkpath $serverdir or die "Cannot make the directory $serverdir!\n";
}
system("tar -xpf server.tar -C $serverdir --overwrite") == 0 or die "Encountered error unzipping the Pace Server tarball!\n";


#Make parent home directory for server
my $paceUserDir = $userHome . "/open-pace";
my $paceServerUserDir = $paceUserDir . "/pace-server/" . $servername;
if (-d $paceServerUserDir) {
     #path exists.
  } else {
	eval { mkpath($paceServerUserDir) };
	if ($@) {
    		die "Couldn't create $paceServerUserDir: $@";
  	}
}



#Make log directory for server
my $logDir = $paceServerUserDir . "/logs/";
unless(-e $logDir or mkdir $logDir){
  die "Unable to create: $logDir directory\n";
}

#Move the transfer to the user's home directory
my $transDir = $paceServerUserDir . "/transfer/";
unless(-e $transDir or mkdir $transDir){
  die "Unable to create: $transDir directory\n";
}

#Move the conf_server to the user's home directory
my $confSvrDir = $paceServerUserDir . "/conf_server/"; 
if (-d $confSvrDir) {
    print $confSvrDir . " already exists, skipping.\n\n";
  } else {
    move($serverdir . "/conf_server", $confSvrDir) or die "Conf_server copy failed: $!"; 
}

#Move the conf to the user's home directory
my $confDir = $paceServerUserDir . "/conf/"; 
if (-d $confDir) {
    print $confDir . " already exists, skipping.\n\n";
  } else {
    move($serverdir . "/conf", $confDir) or die "Conf directory copy failed: $!"; 
}

#if new derby, perform steps here
my $myderbydir = $derbydir . "/data/" . $derbyDbSetName;
if($newderby == 1)
{
  if (-d $derbydir)
  {
    rmtree $derbydir;
  }
  
  unless(-d $derbydir)
  {
    mkpath $derbydir or die "Cannot make the directory $derbydir: $!!\n";
  }

  open (MYFILE, ">>$derbydir/ReliantServerInstances.ini");
  print MYFILE "$derbyname\n";
  close (MYFILE);

  my $cmd = "cp -r " . $serverdir . "/db/* " . $derbydir;
  `$cmd`;
  
  rmtree $derbydir . "/data";
  
  unless(-d $myderbydir)
  {
    mkpath $myderbydir or die "Cannot make the directory $myderbydir: $!!\n";
  }
  move($serverdir. "/db/data", $myderbydir) or die "Copy failed from";

  my $derbylogdir = quotemeta('wrapper.java.additional.4=-Dderby.stream.error.file="C:\ProgramData\Open Pace\Derby\SVR\logs\derby.log"');
  my $derbywrapperdir = quotemeta('wrapper.logfile=C:\ProgramData\Open Pace\Derby\SVR\logs\wrapper.log');
  
  # Install Derby Service
  my $file = $derbydir . "/bin/wrapper.conf";
  my $derbyUserDir = $paceUserDir . "/derby/" . $derbyname;
  my $derbyLog = $derbyUserDir . "/logs"; 
  FileRegEx($file, "wrapper.java.command=\%JAVA_HOME\%/bin/java", "#wrapper.java.command=\%JAVA_HOME\%/bin/java"
	, "#wrapper.java.command=java", "wrapper.java.command=java"
	, "OPEN_PACE_DB_SVR", "OPEN_PACE_DB_" . $derbyname
	, "StartNetworkServer.bat", "StartNetworkServer.sh"
	, "1527", $derbyport
	, $derbylogdir, "wrapper.java.additional.4=-Dderby.stream.error.file=" . $derbyLog . "/derby.log"
	, $derbywrapperdir, "wrapper.logfile=" . $derbyLog . "/wrapper.log");

  if($allowremotederby =~ /yes/i)
  {
    FileRegEx($file, "#wrapper.app.parameter.5", "wrapper.app.parameter.5", "#wrapper.app.parameter.6", "wrapper.app.parameter.6");
  }

  move($derbydir . "/bin/pacederby.sh", $derbydir . "/bin/" . $derbyservicename . ".sh");
  $file = $derbydir . "/bin/InstallDerbyServer.sh";
  FileRegEx($file, "pacederby", $derbyservicename);
  
  $file = $derbydir . "/bin/InstallDerbyServer-RHEL.sh";
  FileRegEx($file, "pacederby", $derbyservicename);

  $file = $derbydir . "/bin/UninstallDerbyServer.sh";
  FileRegEx($file, "pacederby", $derbyservicename);  

  $file = $derbydir . "/UninstallFullDerbyServer.sh";
  FileRegEx($file, "pacederby", $derbyservicename);  
  
  chdir $derbydir . "/bin/";
  
  #Make log directory for Derby
  if (-d $derbyLog) {
    print $derbyLog . " already exists, skipping.\n\n";
  } else {
  	mkpath $derbyLog or die "Cannot make the directory $derbyLog \n\n";
  }
  
  print "\n*** Installing Derby as a service\n";
  if(-e "/etc/debian_version")
  {
    print "Detected a Debian based distribution\n";
	
	#Detect if the user is running this script as root
	if($< == 0)
	{
		system($derbydir . "/bin/InstallDerbyServer.sh");
	}
	else
	{ 
		print "Not running as root. Derby will not be installed as a service.\n"; 
	} 
  }
  else
  {
    print "Detected a Fedora/RedHat based distribution\n";
    
	#Detect if the user is running this script as root
	if($< == 0)
	{
		system($derbydir . "/bin/InstallDerbyServer-RHEL.sh");
	}
	else
	{ 
		print "Not running as root. Derby will not be installed as a service.\n"; 
	} 
  }
}

# Logic for creating new derby databases in an existing derby installation
my $derbySourceDir = $serverdir . "/db";
if($sharedbs == 0 && $noderby == 0 && $newderby == 0)
{
  my $myderbydir = $derbyfileloc . "/data/" . $derbyDbSetName;
  unless(-d $myderbydir)
  {
    mkpath $myderbydir or die "Cannot make the directory $derbydir: $!!\n";
  }
  move($derbySourceDir . "/data", $myderbydir) or die "Copy failed from $serverdir" . "/db/data to $myderbydir: $!";
}

# Update derby references in Client Data Source XML
if($noderby == 0)
{
  my $file = $paceServerUserDir . "/conf_server/paceRDBClientDataSources.xml";
  my $myderbydir = $derbydir . "/data/" . $derbyDbSetName;
  FileRegEx($file, "localhost:1527/../data", $derbyloc . ":" . $derbyport . "/" . $myderbydir);
}


my $serverlogdir = quotemeta('wrapper.app.parameter.12=-Djboss.server.log.dir="C:\ProgramData\Open Pace\Pace Server\SVR\logs"');
my $serverconfdir = quotemeta('wrapper.app.parameter.13=-Dpace.server.conf.dir="C:\ProgramData\Open Pace\Pace Server\SVR"');
my $serverwrapperdir = quotemeta('wrapper.logfile=C:\ProgramData\Open Pace\Pace Server\SVR\logs\wrapper.log');


my $file = $serverdir . "/server/bin/wrapper.conf";
FileRegEx($file, "wrapper.java.command=\%JAVA_HOME\%/bin/java", "#wrapper.java.command=\%JAVA_HOME\%/bin/java"
	, "#wrapper.java.command=java", "wrapper.java.command=java"
	, "wrapper.java.additional.6=-Dprogram.name=run.bat", "#wrapper.java.additional.6=-Dprogram.name=run.bat"
	, "#wrapper.java.additional.6=-Dprogram.name=run.sh", "wrapper.java.additional.6=-Dprogram.name=run.sh"
	, "768", $javaheap
	, $serverlogdir, "wrapper.app.parameter.12=-Djboss.server.log.dir=" . $paceServerUserDir . "/logs"
	, $serverconfdir, "wrapper.app.parameter.13=-Dpace.server.conf.dir=" . $paceServerUserDir
	, $serverwrapperdir, "wrapper.logfile=" . $paceServerUserDir . "/logs/wrapper.log"
	, "\"", "");

$file = $serverdir . "/server/standalone/deployments/pace.war/WEB-INF/classes/log4j.properties";	
FileRegEx($file, "log4j.appender.PACE.file=../standalone/log/pace.log", "log4j.appender.PACE.file=" . $paceServerUserDir . "/logs/pace.log"
			, "log4j.appender.PACEAUDIT.file=../standalone/log/pace-audit.log", "log4j.appender.PACEAUDIT.file=" . $paceServerUserDir . "/logs/pace-audit.log");

$file = $serverdir . "/server/standalone/configuration/standalone.xml";
FileRegEx($file, "jboss.socket.binding.port-offset:0", "jboss.socket.binding.port-offset:$jbossoffset", "\%d{yyyy", 
  "$servername-\%d{yyyy");

###TTN-2427 - Move boot.log
#Make sure the file exists.
unless(-e $file){
  die "File: $file not found\n";
}
#Comment out the exiting entry
#This is done because the existing search and replace won't handle an $
$file = $serverdir . "/server/standalone/configuration/logging.properties";
FileRegEx($file, "handler.FILE.fileName=", "#handler.FILE.fileName=");
#Now add a new entry with the correct path to the end of the file
open(my $fh, '>>', $file) or die "Could not open file '$file' $!";
  say $fh "handler.FILE.fileName=" . $paceServerUserDir . "/logs/boot.log";
close $fh;
###End TTN-2427


$file = $serverdir . "/server/standalone/deployments/pace.war/WEB-INF/cxf.xml";
FileRegEx($file, "/app", "/" . $appname);

$file = $serverdir . "/server/bin/wrapper.conf";
FileRegEx($file, "OPEN_PACE_APP_SVR", "OPEN_PACE_APP_" . $servername, "OPEN_PACE_DB_SVR", "OPEN_PACE_DB_" . $derbyname, "\"", "");

move($serverdir . "/server/bin/paceserver.sh", $serverdir . "/server/bin/" . $serverservicename . ".sh");
$file = $serverdir . "/server/bin/InstallPafServer.sh";
FileRegEx($file, "paceserver", $serverservicename);

$file = $serverdir . "/server/bin/InstallPafServer-RHEL.sh";
FileRegEx($file, "paceserver", $serverservicename);

$file = $serverdir . "/server/bin/UninstallPafServer.sh";
FileRegEx($file, "paceserver", $serverservicename);

$file = $serverdir . "/UninstallFullPaceServer.sh";
FileRegEx($file, "paceserver", $serverservicename);

chdir $serverdir . "/server/bin/";



print "\n*** Installing Pace Server as a service\n";
if(-e "/etc/debian_version")
{
  print "Detected a Debian based distribution\n";
  
  #Detect if the user is running this script as root
	if($< == 0)
	{
		system($serverdir . "/server/bin/InstallPafServer.sh");
	}
	else
	{ 
		print "Not running as root. Pace Server will not be installed as a service.\n"; 
	} 
}
else
{
  print "Detected a Fedora/RedHat based distribution\n";
  
  #Detect if the user is running this script as root
	if($< == 0)
	{
		system($serverdir . "/server/bin/InstallPafServer-RHEL.sh");
	}
	else
	{ 
		print "Not running as root. Pace Server will not be installed as a service.\n"; 
	} 
}

my $src = "";
my $srcdir = "";

#Replace the service wrappers with the Linux versions
print "\n*** Installing the appropriate service wrappers\n";
if($architecture =~ /\w*64\w*/)
{
  print "Detected a 64-bit OS\n";
  for $srcdir (grep -d , glob $serverdir . "/redist/service*/wrapper-linux-x86-64*") 
  {
    $src = $srcdir . "/" . "lib/wrapper.jar";
	if($newderby == 1)
	{
      copy($src, $derbydir . "/lib/") or die "Copy failed: $!"; 
	}
    copy($src, $serverdir . "/server/") or die "Copy failed: $!"; 
  }
}
else
{
  print "Detected a 32-bit OS\n";
  for $srcdir (grep -d , glob $serverdir . "/redist/service*/wrapper-linux-x86-32*") 
  {
    $src = $srcdir . "/lib/wrapper.jar";
	if($newderby == 1)
	{
      copy($src, $derbydir . "/lib/") or die "Copy failed: $!"; 
	}
    copy($src, $serverdir . "/server/") or die "Copy failed: $!"; 

    $src = $srcdir . "/lib/libwrapper.so";	
	if($newderby == 1)
	{
      copy($src, $derbydir . "/lib/") or die "Copy failed: $!"; 
	}
    copy($src, $serverdir . "/server/") or die "Copy failed: $!";

    $src = $srcdir . "/bin/wrapper";
	if($newderby == 1)
	{
      copy($src, $derbydir . "/bin/") or die "Copy failed: $!"; 
	}
    copy($src, $serverdir . "/server/bin/") or die "Copy failed: $!";
  }
}

# Clean-up (remove) derby install directory from server install directory
if (-d $derbySourceDir) 
{
	rmtree $derbySourceDir
}

print "\nFinished successfully!\n\n";
exit 0;

sub FileRegEx
{
  my $file = shift(@_);
  open (FILE, "<" . $file) or die "Can't open $file: $!\n";
  my @lines = <FILE>;
  close FILE;
  my $OUTFILE;
  open $OUTFILE, '>', $file or die "Cannot open $file: $!\n";
  my $prev = "";
  my $post = "";
  my $count = scalar(@_);
  my $ndx = 0;
  for ( @lines ) 
  {
    for($ndx = 0; $ndx < $count; $ndx = $ndx+2)
    {
      $prev = $_[$ndx];
      $post = $_[$ndx+1];
      $_ =~ s/$prev/$post/ig;
    }
    print $OUTFILE $_ or die "Cannot print to file: $!";
  }
  close $OUTFILE or die "Cannot close $file: $!";
}

sub UserInput
{
  while(<STDIN>)
  {
    my $temp = $_;
    $temp =~ s/[\n\r\f\t]//g;
    foreach(@_)
    {
      if($temp =~ /$_/i)
      {
        if($_ =~ /q/i)
        {
          print "\n\n*** Quitting the install before completion ***\n\n";
          exit 0;
        }
        return $_;
      }
    }
    $temp = "Please enter ";
    foreach(@_)
    {
      $temp = $temp . "$_, ";
    }
    chop $temp;
    chop $temp;
    print "$temp: ";
  }
}

sub CountSubDirs
{
  my $source = $_[0]; 
  my $folders = 0;
  my $files = 0;

  opendir (SOURCE, $source) or return 0; 

  my $file; 
  while (defined($file = readdir(SOURCE)))
  { 
    next if $file =~ /^\.{1,2}$/; 
    my $full_name = "$source/$file"; 
    if (-d $full_name)
    { 
      $folders++;
    }
  }
  return $folders;
}
