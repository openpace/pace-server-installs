﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BuildIncrement
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                string fileName = args[0];
                string prodVers = args[1];
				string licensePageId = args[2];

                string newFileText = "";

                using (StreamReader reader = new StreamReader(fileName))
                {
                    try
                    {
                        switch (Path.GetExtension(fileName))
                        {
                            case ".ism": //installshield installer project file
                                {
                                    string file = reader.ReadToEnd();
                                    string pattern = @"ProductVersion</td><td>\d*.\d*.\d*.\d*";
                                    Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                                    MatchCollection matches = rgx.Matches(file);
                                    if (matches.Count > 0)
                                    {
                                        newFileText = file;
                                        foreach (Match match in matches)
                                        {
                                            newFileText = newFileText.Replace(match.Value, "ProductVersion</td><td>" + prodVers);
                                        }
                                    }
                                    break;
                                }
                            case ".pl": //perl linux installer file
                                {
                                    string file = reader.ReadToEnd();
                                    string versions = prodVers.Replace(".", string.Empty).Substring(0, 3);

                                    //Update the displayed version from the perl script
                                    string pattern = @"Version\s\d*\.\d*\.\d*\.\d*";
                                    Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                                    MatchCollection matches = rgx.Matches(file);
                                    if (matches.Count > 0)
                                    {
                                        newFileText = file;
                                        foreach (Match match in matches)
                                        {
                                            newFileText = newFileText.Replace(match.Value, "Version " + prodVers);
                                        }
                                    }

                                    //Update the wiki location for the EULA
                                    // pattern = @"/wiki/display/PPD\d\d\d";
									// https://pace-planning.atlassian.net/wiki/spaces/PPD300/pages/162988084/License
									pattern = @"/wiki/spaces/PPD\d\d\d/pages/\d+/License";
                                    rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                                    matches = rgx.Matches(file);
                                    if (matches.Count > 0)
                                    {
                                        foreach (Match match in matches)
                                        {
                                            // newFileText = newFileText.Replace(match.Value, "/wiki/display/PPD" + versions);
											newFileText = newFileText.Replace(match.Value, "/wiki/spaces/PPD300/pages/" + licensePageId + "/License");
                                        }
                                    }

                                    break;
                                }
                        }
                    }
                    catch (Exception) { }
                }

                using (StreamWriter writer = new StreamWriter(fileName, false))
                {
                    writer.Write(newFileText);
                }
            }
        }
    }
}
